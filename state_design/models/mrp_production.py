# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import ValidationError
from datetime import datetime


class MrpProduction(models.Model):
    _inherit = "mrp.production"

    status_design = fields.Selection(
        selection=[
            ("normal", "To approve"),
            ("done", "Approved"),
        ],
        default="normal",
        required=True,
    )
    date_hour = fields.Datetime(
        string="En diseño desde",
        readonly=True,
    )

    def button_design(self):
        self.status_design = "done"
        self.date_hour = datetime.now()

    def button_plan(self):
        for production in self:
            if production.status_design != "done":
                raise ValidationError(
                    _(f"The production {production.name} is not approved by design")
                )
        res = super(MrpProduction, self).button_plan()
        return res
