# -*- coding: utf-8 -*-
{
    "name": "State Design",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://gitlab.com/HomebrewSoft.dev",
    "license": "LGPL-3",
    "depends": [
        "mrp",
    ],
    "data": [
        # security
        # data
        # reports
        # views
        "views/mrp_production.xml",
    ],
}
